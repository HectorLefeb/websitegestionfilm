<!DOCTYPE html>
<html lang="en">
<head>
  <title>Page Accueil</title>
  <link rel="stylesheet" href="sitecss.css" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
    <?php require "header.php"; ?>
    <div class="flip-card-container">
        <?php
            $i = 0;
            $result = $file_db->query('SELECT * FROM GENRE');
            foreach ($result as $m){$i+=1;?>
                <?php $image = $file_db->query("SELECT lienPhoto FROM FILM WHERE idGenre = $m[idGenre]");
                foreach ($image as $im){$i = $i;}?>
                <div class='boite'>
                    <div class='flip-card'>
                        <div class='flip-card-inner'>
                            <div class='flip-card-front'>
                                <img src="<?php echo"$im[lienPhoto]" ?>" alt="img <?php echo"$i" ?>" style="width:300px;height:400px;">
                            </div>
                            <div class="flip-card-back">
                                <h1>Genre</h1>
                                <p><?php echo"$m[nomGenre]"?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
    <?php $file_db = null; ?>
</body>
</html>
