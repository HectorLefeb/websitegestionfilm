<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <!-- Brand -->
        <a class="navbar-brand" href="site.php">Accueil</a>

        <!-- Links -->
        <ul class="navbar-nav">
            <li class="nav-item">
            <a class="nav-link" href="pageExplorer.php">Explorer</a>
            </li>
            <?php
                $file_db = new PDO("sqlite:../bd/film.sqlite");
                $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $result = $file_db->query('SELECT * FROM GENRE');
                    foreach ($result as $m){
                        echo "<li class='nav-item'><a class='nav-link' href='pageParGenre.php?idGenre=$m[idGenre]&nomGenre=$m[nomGenre]'>$m[nomGenre]</a></li>";
                    }
            ?>
        </ul>
        <form class="form-inline" action="/action_page.php">
            <input class="form-control mr-sm-2" type="text" placeholder="Search">
            <button class="btn btn-success" type="submit">Search</button>
        </form>
        <li class='nav-item-add'><a class='nav-link-add' href='ajouterFilm.php'>Ajouter Film</a></li>
    </nav>